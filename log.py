import logging
import datetime
import collections

class Logger:
    def __init__(self, app):
        self.app = app
        self.logQueue = collections.deque(maxlen=256)

    def info(self, msg, *args, **kwargs):
        """
        Delegate a info log call to the underlying logger,
        """
        s = msg % args
        self.logQueue.append(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " [INFO] : " + s)
        print(s)
        self.app.logger.info(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        """
        Delegate a error log call to the underlying logger,
        """
        s = msg % args
        self.logQueue.append(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " [FAIL] : " + s)
        print(s)
        self.app.logger.error(msg, *args, **kwargs)