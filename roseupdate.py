from flask import Flask, request, make_response, render_template, send_from_directory, redirect, url_for
import logging
import datetime
import subprocess
import sys
import threading
import time
import json
import os
import traceback
import pprint
import shutil
from utils import copy
from log import Logger
from xmlrpc.client import ServerProxy

version = '0.1.5'
status = 'started'
base_roseguarden_path = "/home/roseguarden/"

def abort_update(logger):
    logger.error("Update got aborted")
    logger.error("Please restart manually")
    while True:
        time.sleep(15)

def shutdown(logger):
    # shutdown roseupdate
    logger.info("Update finished, going to shutdown roseupdate")
    time.sleep(5)    
    try:
        server = ServerProxy('http://localhost:9001/RPC2')
        server.supervisor.stopProcess('roseupdate')
        status = 'succeed'
    except Exception as e:
        logger.error("Failed to stop roseupdate %s", str(e))    

def setup_backend(logger, reponame, destination):
    logger.info("Copy fresh " + reponame + " backend to production directory")
    copy(logger, base_roseguarden_path + "repositories/" + reponame, base_roseguarden_path + "/" + destination)

    logger.info("Install python requirements with pip")
    try:
        proc = subprocess.Popen(['pip3 install -r requirements.txt'], shell=True, cwd= base_roseguarden_path + destination, stdout=subprocess.PIPE)
    except BaseException as e:
        logger.error("Install requirements failed: " + str(e))

    while True:
        retcode = proc.poll()
        if retcode != None:
            if retcode is 0:
                logger.info("Install process finished successful")
            else:
                outs, errs = proc.communicate()
                logger.warning("Install process finished with error (code=%s)", str(retcode))
                if errs is not None:
                    logger.error("Error:\n%s", str(errs.decode('UTF-8')))
                if outs is not None:
                    logger.warning("Output:\n%s", str(outs.decode('UTF-8')))         
            break
        else:
            logger.info("Install process is still running")

        time.sleep(15)

def setup_build_enviroment(logger):
    # create build directory if not exist
    try:
        buildDir = "../build"
        os.mkdir(buildDir)
        print("Build directory " , buildDir ,  " created ") 
    except FileExistsError:
        print("Build directory " , buildDir ,  " already exists")

def get_repository_info(logger):
    # read repositprie informations
    try:
        json_file = open(base_roseguarden_path + "repositories/repositories.txt", "r")
        repoinfo = json.load(json_file)
        json_file.close()
        return repoinfo
    except BaseException as e:
        logger.error("Broken repository file", str(e))
        return None

def build_frontend(logger, repo):
    logger.info("Copy fresh " + repo + " to the build directory")
    copy(logger, base_roseguarden_path + "repositories/" + repo, base_roseguarden_path + "build/" + repo)

    logger.info("Install " + repo + " environment (this could time up to 10min)")
    try:
        proc = subprocess.Popen(['npm install'], shell=True, cwd=base_roseguarden_path + "build/" + repo, stdout=subprocess.PIPE)
    except BaseException as e:
        logger.error("Start install process failed: " + str(e))

    while True:
        retcode = proc.poll()
        if retcode != None:
            if retcode is 0:
                logger.info("Install process finished successful")
            else:
                outs, errs = proc.communicate()
                logger.error("Install process finished with error (code=%s)", str(retcode))
                if errs is not None:
                    logger.error("Error:\n%s", str(errs.decode('UTF-8')))
                if outs is not None:
                    logger.error("Output:\n%s", str(outs.decode('UTF-8')))
            break
        else:
            logger.info("Install process is still running")
        time.sleep(30)

    logger.info("Build " + repo + " (this could time up to 5min)")
    try:
        proc = subprocess.Popen(['npm run build'], shell=True, cwd=base_roseguarden_path + "build/" + repo, stdout=subprocess.PIPE)
    except BaseException as e:
        logger.error("Start build process failed: " + str(e))

    while True:
        retcode = proc.poll()
        if retcode != None:
            if retcode is 0:
                logger.info("Install process finished with successful")
            else:
                outs, errs = proc.communicate()
                logger.error("Install process finished with error (code=%s)", str(retcode))
                if errs is not None:
                    logger.error("Error:\n%s", str(errs.decode('UTF-8')))
                if outs is not None:
                    logger.error("Output:\n%s", str(outs.decode('UTF-8')))
            break
        else:
            logger.info("Install process is still running")
        time.sleep(30)

def print_needed_updates(logger, repoinfo):

    if "roseguarden.backend" in repoinfo:
        if repoinfo["roseguarden.backend"]["update"]:
            logger.info("Module roseguarden.backend need an update")
        else:
            logger.info("Module roseguarden.backend is up to date")
    else:
        logger.error("No roseguarden.backend infos found in updatefile")

    if "roseguarden.frontend" in repoinfo:
        if repoinfo["roseguarden.frontend"]["update"]:
            logger.info("Module roseguarden.frontend need an update")
        else:
            logger.info("Module roseguarden.frontend is up to date")
    else:
        logger.error("No roseguarden.frontend infos found in updatefile")


    if "rosenodemock" in repoinfo:
        if repoinfo["rosenodemock"]["update"]:
            logger.info("Module rosenodemock need an update")
        else:
            logger.info("Module rosenodemock is up to date")
    else:
        logger.error("No rosenodemock infos found in updatefile")

    if "rosenodemock.frontend" in repoinfo:
        if repoinfo["rosenodemock.frontend"]["update"]:
            logger.info("Module rosenodemock.frontend need an update")
        else:
            logger.info("Module rosenodemock.frontend is up to date")
    else:
        logger.error("No rosenodemock.frontend infos found in updatefile")


def update_rosemocknode(logger, repoinfo):
    if "rosenodemock" not in repoinfo or "rosenodemock.frontend" not in repoinfo:
        return 

    logger.info("Update rosenodemock ...")

    if repoinfo["rosenodemock.frontend"]["update"] == True:
        logger.info("Delete rosenodemock.frontend build directory ...")
        try:
            shutil.rmtree(base_roseguarden_path + 'build/rosenodemock.frontend')
        except BaseException as e:
            logger.error("Delete rosenodemock.frontend build directory failed: " + str(e))

        try:
            build_frontend(logger, "rosenodemock.frontend")
        except Exception as e:
            logger.error("Build rosenodemock.frontend failed: " + str(e))

    if repoinfo["rosenodemock"]["update"] == True:
        logger.info("Delete rosenodemock backend...")
        try:
            shutil.rmtree(base_roseguarden_path + 'rosenodemock')
        except BaseException as e:
            logger.error("Delete rosenodemock backend failed: " + str(e))

        setup_backend(logger, "rosenodemock", "rosenodemock")

    if os.path.exists(base_roseguarden_path + 'rosenodemock/dist'):
        logger.info("Delete rosenodemock frontend...")
        try:
            shutil.rmtree(base_roseguarden_path + 'rosenodemock/dist')
        except BaseException as e:
            logger.error("Delete rosenodemock frontend failed: " + str(e))

    logger.info("Copy fresh rosenodemock frontend to the production directory")
    try:
        copy(logger, base_roseguarden_path + "build/rosenodemock.frontend/dist", base_roseguarden_path + "rosenodemock/dist")
    except BaseException as e:
        logger.error("Copy roseguarden frontend failed: " + str(e))             

def update_roseguarden(logger, repoinfo):

    if "roseguarden.backend" not in repoinfo or "roseguarden.frontend" not in repoinfo:
        return 

    logger.info("Update roseguarden ...")

    if repoinfo["roseguarden.frontend"]["update"] == True:
        logger.info("Delete roseguarden.frontend build directory ...")
        try:
            shutil.rmtree(base_roseguarden_path + 'build/roseguarden.frontend')
        except BaseException as e:
            logger.error("Delete roseguarden.frontend build directory failed: " + str(e))

        try:
            build_frontend(logger, "roseguarden.frontend")
        except Exception as e:
            logger.error("Build roseguarden.frontend failed: " + str(e))

    if repoinfo["roseguarden.backend"]["update"] == True:
        logger.info("Delete roseguarden backend...")
        try:
            shutil.rmtree(base_roseguarden_path + 'roseguarden')
        except BaseException as e:
            logger.error("Delete roseguarden backend failed: " + str(e))

        setup_backend(logger, "roseguarden.backend", "roseguarden")

    if os.path.exists(base_roseguarden_path + 'roseguarden/dist'):
        logger.info("Delete roseguarden frontend...")
        try:
            shutil.rmtree(base_roseguarden_path + 'roseguarden/dist')
        except BaseException as e:
            logger.error("Delete roseguarden frontend failed: " + str(e))

    logger.info("Copy fresh roseguarden frontend to the production directory")
    try:
        copy(logger, base_roseguarden_path + "build/roseguarden.frontend/dist", base_roseguarden_path + "roseguarden/dist")
    except BaseException as e:
        logger.error("Copy roseguarden frontend failed: " + str(e))

def update_worker(logger):
    status = 'running'
    try:
        time.sleep(2)
        logger.info("Started update worker ...")

        setup_build_enviroment(logger)

        repoinfo = get_repository_info(logger)
        if repoinfo == None:
            abort_update(logger)

        print_needed_updates(logger, repoinfo)

        update_rosemocknode(logger, repoinfo)

        update_roseguarden(logger, repoinfo)

        logger.info("Shutdown the updater")

        shutdown(logger)

        sys.exit(0)
    except Exception as e:
        logger.info("Update failed, please stop the updater manually \n")
        logger.error("Error: " + (str(e)))
        logger.error("Traceback: \n" + str(traceback.format_exc()))
        status = 'failed'
        while True:
            time.sleep(5)



# initialize flask app
app = Flask(__name__,
            static_folder = "./dist")

logger = Logger(app)

# define routes for the flask app 
@app.route('/index')
@app.route('/')
def index():
    return send_from_directory('dist', 'index.html')
    #return render_template("index.html")

@app.route('/<path:path>')
def static_proxy(path):
    if (os.path.isdir("./dist/" + path)):
        return send_from_directory('dist/' + path, 'index.html')
    elif (os.path.isfile("./dist/" + path)):
        return app.send_static_file(path)
    else:
        return redirect(url_for('index'))

@app.route('/api/v1/log', methods=["GET"])
def getLog():
    log_string = ""
    for e in logger.logQueue:
        log_string = log_string + e + "\n"
    return log_string


@app.route('/api/v1/nodes', methods=["POST"])
def nodeTest():
    print(pprint.pformat(request.json, indent=2))
    logger.info("node request from: {}\n{}".format(request.remote_addr, pprint.pformat(request.json, indent=2)))    
    return make_response("ready for test\n", 200)    

@app.route('/api/v1/version', methods=["GET"])
def getVersion():
    return version

@app.route('/api/v1/updatestatus', methods=["GET"])
def getStatus():
    return status


# start background worker
if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
    logger.info("Start roseupdate v"+ str(version))
    worker = threading.Thread(target=update_worker, args=(logger,))
    worker.setDaemon(True)
    worker.start()


if __name__ != '__main__':
    # get logging from unicorn
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    logger.info("Run app with gunicorn with update worker")

    # app.logger.addHandler(errhandler)

if __name__ == '__main__':
    logger.info("Run app locally without worker (test)")
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.WARNING)    
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=False)


# wait for bacground worker to finish
# worker.join()


