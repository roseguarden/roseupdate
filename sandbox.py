import collections

def info(msg, *args, **kwargs):
    s = msg % args
    print(s)

# info("%d sadsd", 1)

d = collections.deque(maxlen=10)

for i in range(12):
    d.append(i)

for i in d:
    print(i)
